from flask import Flask, request, url_for, render_template, redirect, flash
import mysql.connector

con = mysql.connector.connect(
    host="127.0.0.1",
    user="root",
    password="",
    database="employee"
)
app = Flask(__name__)
app.config.from_object(__name__)
app.config["MYSQL_CURSORCLASS"] = "DictCursor"
#-----------------------------------------INDEX FILE--------------------------------------------------------------
@app.route('/')
def index():
    db = con.cursor()
    sql = "SELECT * FROM emp_detail"
    db.execute(sql)
    result = db.fetchall()
    return render_template('index.html', datas=result)

@app.route('/salaryindex')
def salaryindex():
    db = con.cursor()
    sql = "SELECT * FROM monthly_salary"
    db.execute(sql)
    result = db.fetchall()
    return render_template('salary_detail_index.html', datas=result)

#______________________________________________ADD EMPLOYEE DETAIL_______________________________________________

@app.route('/Add_emp', methods=["GET", "POST"])
def Add_emp():
    if request.method == 'POST':
        name = request.form['name']
        address = request.form['address']
        gender = request.form['gender']
        join_date = request.form['date']
        createdtime = request.form['time1']
        updatedtme = request.form['time2']
        db = con.cursor()
        sql = "insert into emp_detail(NAME,ADDRESS,GENDER,JOINEDDATE,CREATEDTIME,UPDATEDTIME) value (%s,%s,%s,%s,%s,%s)"
        db.execute(sql, [name, address, gender, join_date, createdtime, updatedtme])
        con.commit()
        db.close()
        return redirect(url_for("index"))
    return render_template("Add.html")

@app.route('/edit/<id>', methods=["GET","POST"])
def edit(id):
    db=con.cursor()
    if request.method == 'POST':
        name = request.form['name']
        address = request.form['address']
        gender = request.form['gender']
        joindate = request.form['joindate']
        createdtime = request.form['createdtime']
        updatedtme = request.form['updatedtime']
        sql = "update emp_detail set NAME=%s,ADDRESS=%s,GENDER=%s,JOINEDDATE=%s,CREATEDTIME=%s,UPDATEDTIME=%s where ID=%s"
        db.execute(sql, [name, address, gender, joindate, createdtime, updatedtme, id])
        con.commit()
        db.close()
        return redirect(url_for("index"))
    db.execute("select * from emp_detail where ID=%s", [id])
    result = db.fetchone()
    return render_template("edit.html", datas=result)


@app.route('/delete/<id>', methods=["GET", "POST"])
def delete(id):
    db = con.cursor()
    db.execute("delete from emp_detail where ID=%s", [id])
    con.commit()
    db.close()
    return redirect(url_for("index"))
#---------------------------------------SALARY DETAIL--------------------------------------------------------------------
@app.route('/salarybutton', methods=["GET", "POST"])
def salarybutton():
    return render_template("salary_detail_index.html")


@app.route('/add_salary', methods=["GET","POST"])
def add_salary():
    if request.method == 'POST':
        emp_id = request.form['emp_id']
        month = request.form['month']
        salaryy = request.form['salaryy']
        db = con.cursor()
        sql = "insert into monthly_salary(EMPLOYEEID,MONTH,SALARY) values (%s,%s,%s)"
        db.execute(sql, [emp_id, month, salaryy])
        con.commit()
        db.close()
        return redirect(url_for("salaryindex"))
    return render_template("add_salary_every_month.html")

@app.route('/salaryedit/<id>', methods=["GET","POST"])
def salaryedit(id):
    db=con.cursor()
    if request.method == 'POST':
        emp_id = request.form['emp_id']
        month = request.form['month']
        salaryupdate = request.form['salary']
        createdtime = request.form['createdtime']
        updatedtme = request.form['updatedtime']
        sql = "update monthly_salary set EMPLOYEEID=%s,MONTH=%s,SALARY=%s,CREATEDTIME=%s,UPDATEDTIME=%s where ID=%s"
        db.execute(sql, [emp_id, month, salaryupdate, createdtime, updatedtme])
        con.commit()
        db.close()
        return redirect(url_for("salaryindex"))
    db.execute("select * from monthly_salary where ID=%s", [id])
    result = db.fetchone()
    return render_template("salaryedit.html", datas=result)

@app.route('/salarydelete/<id>', methods=["GET", "POST"])
def salarydelete(id):
    db = con.cursor()
    db.execute("delete from monthly_salary where ID=%s", [id])
    con.commit()
    db.close()
    return redirect(url_for("salaryindex"))

if __name__=="__main__":
    app.run(debug=True)